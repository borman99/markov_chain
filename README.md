# Сборка
Проверялась в конфигурации:
- Mac OS X, Clang 3.5, boost 1.58

# Опции командной строки
```sh
$ ./markov-build --help
Allowed options:
  --output arg          Output file name
  --n arg (=3)          N for n-gram
  --help                show this message
  --info                info logging
  --verbose             verbose logging
  --very-verbose        very verbose logging

$ ./markov-generate --help
Allowed options:
  --input arg           Output file name
  --seed arg (=0)       Random seed (randomize if 0)
  --help                show this message
  --info                info logging
  --verbose             verbose logging
  --very-verbose        very verbose logging
```

# Пример работы
```sh
$ cat ../lotr.txt
http://www.ae-lib.org.ua/texts-c/tolkien__the_lord_of_the_rings_1__en.htm
http://www.ae-lib.org.ua/texts-c/tolkien__the_lord_of_the_rings_2__en.htm
http://www.ae-lib.org.ua/texts-c/tolkien__the_lord_of_the_rings_3__en.htm

$ ./markov-build --n=4 --output=lotr4.bin < ../lotr.txt --info
[INFO]  BEGIN BUILDER (N = 4)
[INFO]  FETCH http://www.ae-lib.org.ua/texts-c/tolkien__the_lord_of_the_rings_1__en.htm
[INFO]  DONE http://www.ae-lib.org.ua/texts-c/tolkien__the_lord_of_the_rings_1__en.htm
[INFO]  FETCH http://www.ae-lib.org.ua/texts-c/tolkien__the_lord_of_the_rings_2__en.htm
[INFO]  DONE http://www.ae-lib.org.ua/texts-c/tolkien__the_lord_of_the_rings_2__en.htm
[INFO]  FETCH http://www.ae-lib.org.ua/texts-c/tolkien__the_lord_of_the_rings_3__en.htm
[INFO]  DONE http://www.ae-lib.org.ua/texts-c/tolkien__the_lord_of_the_rings_3__en.htm
[INFO]  END BUILDER
[INFO]  BEGIN SAVE DATA (lotr4.bin)
[INFO]  END SAVE DATA

$ echo 'but he knew that 20' | ./markov-generate --input=lotr4.bin
he beheld things as they were the light was even clearer and stronger 'i saw a dark bird-like figure wheel

$ echo 'but he knew that 20' | ./markov-generate --input=lotr4.bin
they had quite forgotten it it came to it difficult to daunt or to kill and avenge our folk i

$ echo 'but he knew that 20' | ./markov-generate --input=lotr4.bin
all round him were the companies of the enemy - and he's under orders of some kind the other was
```
