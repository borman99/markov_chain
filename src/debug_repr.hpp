#pragma once

#include <ostream>
#include <vector>

namespace my { namespace debug_repr {

template<typename T>
std::ostream& operator <<(std::ostream& stream, const std::vector<T>& value) {
    stream << '[';
    auto first = true;
    for (auto& item : value) {
        if (!first) {
            stream << ", ";
        } else {
            first = false;
        }

        stream << item;
    }
    stream << ']';

    return stream;
}

}} // namespace my::debug_repr
