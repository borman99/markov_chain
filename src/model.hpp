#pragma once

#include <string>
#include <vector>

#include "trie.hpp"
#include "string_pool.hpp"

namespace my { namespace model {

struct ngram_data {
    using word_trie = trie<
        string_pool::id_type,
        string_pool::count_type
    >;

    size_t n;
    std::vector<std::string> strings;
    word_trie ngrams;
};

void print_node(const ngram_data& data, word_trie* node, size_t level) {
    if (level < n-1) {

    } else {
        // leaf
    }
}

std::ostream &operator <<(std::ostream& stream, const ngram_data& data) {

    return stream;
}

}} // namespace my::model
