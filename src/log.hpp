#pragma once

#include <sstream>
#include <iostream>
#include <cerrno>
#include <cstring>

#include "term.hpp"
#include "debug_repr.hpp"

namespace my { namespace log {

extern bool enable_info;
extern bool enable_debug;
extern bool enable_trace;


inline
void format(std::ostream&) {
}

template<typename T, typename... U>
inline
void format(std::ostream& stream, T&& first, U&&... rest) {
    using namespace debug_repr;

    stream << first;
    format(stream, rest...);
}

template<typename... T>
inline
void println(T&&... args) {
    std::ostringstream line;
    format(line, args..., '\n');
    std::cout << line.str();
}

template<typename... T>
inline
void trace(T&&... args) {
    if (enable_trace) {
        println(
            term::BLUE,
            "[TRACE] ", args...,
            term::RESET
        );
    }
}

template<typename... T>
inline
void debug(T&&... args) {
    if (enable_debug) {
        println(
            term::GREEN,
            "[DEBUG] ", args...,
            term::RESET
        );
    }
}

template<typename... T>
inline
void info(T&&... args) {
    if (enable_info) {
        println("[INFO]  ", args...);
    }
}

template<typename... T>
inline
void error(T&&... args) {
    println(
        term::RED,
        "[ERROR] ", args...,
        term::RESET
    );
}

template<typename... T>
inline
void perror(T&&... args) {
    auto error_message = ::strerror(errno);
    error(args..., ": ", error_message);
}

template<typename... T>
inline
void ensure(bool condition, T&&... args) {
    if (!condition) {
        println(
            term::RED,
            "[FATAL] Assertion failed: ", args...,
            term::RESET
        );
        throw std::runtime_error("assertion failed");
    }
}


}} // namespace my::log
