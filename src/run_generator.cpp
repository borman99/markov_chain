#include <fstream>
#include <vector>
#include <random>

#include <boost/program_options.hpp>
#include <boost/archive/binary_iarchive.hpp>

#include "model/data.hpp"
#include "model/generator.hpp"
#include "errno_exception.hpp"
#include "log.hpp"

namespace po = boost::program_options;

using data32 = my::model::basic_data<uint32_t, uint32_t, uint32_t>;

int unsafe_main(int argc, char* argv[]) {
    std::ios_base::sync_with_stdio(false);

    bool opt_help;
    uint64_t opt_seed;
    std::string opt_input;

    po::options_description options("Allowed options");
    options.add_options()
        ("input",
         po::value(&opt_input)->required(),
         "Output file name")
        ("seed",
         po::value(&opt_seed)->default_value(0),
         "Random seed (randomize if 0)")
        ("help",
         po::bool_switch(&opt_help),
         "show this message")
        ("info",
         po::bool_switch(&my::log::enable_info),
         "info logging")
        ("verbose",
         po::bool_switch(&my::log::enable_debug),
         "verbose logging")
        ("very-verbose",
         po::bool_switch(&my::log::enable_trace),
         "very verbose logging")
    ;

    try {
        po::variables_map var_map;
        po::store(po::command_line_parser(argc, argv)
                  .options(options)
                  .run(),
                  var_map);
        po::notify(var_map);
    } catch (const std::exception& err) {
        my::log::error("Error: ", err.what());
        std::clog << options;
        return 1;
    }

    if (opt_help) {
        std::clog << options;
        return 0;
    }

    auto data = data32 {};

    my::log::info("BEGIN LOAD DATA (", opt_input, ")");
    std::ifstream input(opt_input, std::ifstream::binary);
    if (!input.is_open()) {
        my::log::error("Failed to open ", opt_input);
        throw my::errno_exception();
    }
    boost::archive::binary_iarchive input_archive(input);
    input_archive >> data;
    input.close();
    my::log::info("END LOAD DATA");

    // Randomize seed
    if (opt_seed == 0) {
        std::random_device rnd;
        opt_seed = rnd();
    }

    my::log::info("BEGIN READ WORDS (n=", data.n, ")");
    auto generator = make_generator(data);
    auto word = std::string {};
    for (auto i = size_t {0}; i < data.n; ++i) {
        std::cin >> word;
        my::log::debug("read: ", word);
        auto word_id = data.strings.find(word);
        if (!word_id) {
            my::log::error("Unknown word: ", word);
            return 1;
        }
        my::log::trace("found: ", *word_id, " = ", data.strings[*word_id]);
        generator.advance(*word_id);
    }
    my::log::info("END READ WORDS");

    auto rng = std::mt19937_64 {opt_seed};
    auto nwords = size_t {0};
    std::cin >> nwords;
    for (auto i = size_t {0}; i < nwords; ++i) {
        auto word_id = generator.next(rng);
        if (!word_id) {
            break;
        }

        std::cout << data.strings[*word_id] << ' ';
    }
    std::cout << '\n';

    return 0;
}

int main(int argc, char* argv[]) {
    try {
        return unsafe_main(argc, argv);
    } catch (const std::exception& err) {
        my::log::error("Error: ", err.what());
        return 1;
    } catch (...) {
        my::log::error("Unknown error");
        return 1;
    }
}
