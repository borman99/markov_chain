#pragma once

#define DEFINE_ANSI_COLOR(name, value) \
    constexpr const char* name = "\033[" #value "m"

namespace my { namespace term {

DEFINE_ANSI_COLOR(RED, 31);
DEFINE_ANSI_COLOR(GREEN, 32);
DEFINE_ANSI_COLOR(BLUE, 34);
DEFINE_ANSI_COLOR(RESET, 0);

}} // namespace my::term
