#include "log.hpp"

namespace my { namespace log {

bool enable_info = false;
bool enable_debug = false;
bool enable_trace = false;

}} // namespace my::log
