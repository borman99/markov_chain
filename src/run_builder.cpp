#include <fstream>
#include <vector>

#include <boost/program_options.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include "model/builder.hpp"
#include "model/data.hpp"
#include "errno_exception.hpp"
#include "log.hpp"

namespace po = boost::program_options;

using builder32 = my::model::basic_builder<uint32_t, uint32_t, uint32_t>;

int unsafe_main(int argc, char* argv[]) {
    std::ios_base::sync_with_stdio(false);

    bool opt_help;
    size_t opt_n;
    std::string opt_output;

    po::options_description options("Allowed options");
    options.add_options()
        ("output",
         po::value(&opt_output)->required(),
         "Output file name")
        ("n",
         po::value(&opt_n)->default_value(3),
         "N for n-gram")
        ("help",
         po::bool_switch(&opt_help),
         "show this message")
        ("info",
         po::bool_switch(&my::log::enable_info),
         "info logging")
        ("verbose",
         po::bool_switch(&my::log::enable_debug),
         "verbose logging")
        ("very-verbose",
         po::bool_switch(&my::log::enable_trace),
         "very verbose logging")
    ;

    try {
        po::variables_map var_map;
        po::store(po::command_line_parser(argc, argv)
                  .options(options)
                  .run(),
                  var_map);
        po::notify(var_map);
    } catch (const std::exception& err) {
        my::log::error("Error: ", err.what());
        std::clog << options;
        return 1;
    }

    if (opt_help) {
        std::clog << options;
        return 0;
    }

    my::log::info("BEGIN BUILDER (N = ", opt_n, ")");
    auto builder = builder32(opt_n);
    auto line = std::string();
    while (getline(std::cin, line)) {
        if (line.empty()) {
            continue;
        }
        // XXX: Could have used a thread pool here to speed up I/O
        builder.fetch_and_consume(line);
    }
    auto data = builder.produce();
    my::log::trace("data:\n", data);
    my::log::info("END BUILDER");

    my::log::info("BEGIN SAVE DATA (", opt_output, ")");
    std::ofstream output(opt_output, std::ofstream::binary);
    if (!output.is_open()) {
        my::log::error("Failed to open ", opt_output);
        throw my::errno_exception();
    }
    boost::archive::binary_oarchive output_archive(output);
    output_archive << data;
    output.close();
    my::log::info("END SAVE DATA");

    return 0;
}

int main(int argc, char* argv[]) {
    try {
        return unsafe_main(argc, argv);
    } catch (const std::exception& err) {
        my::log::error("Error: ", err.what());
        return 1;
    } catch (...) {
        my::log::error("Unknown error");
        return 1;
    }
}
