#pragma once

#include <cstdio>

#include <boost/utility/string_ref.hpp>

#include "errno_exception.hpp"
#include "log.hpp"

namespace my { namespace curl {

class pipe {
    FILE *stream_;

public:
    pipe(boost::string_ref url) {
        auto cmdline = std::string("curl --silent --fail --location ");
        cmdline.append(url.begin(), url.end());

        stream_ = ::popen(cmdline.c_str(), "r");
        if (!stream_) {
            throw errno_exception();
        }
    }

    bool close() {
        auto retval = ::pclose(stream_);
        stream_ = nullptr;
        return retval == 0;
    }

    size_t read(char* buf, size_t count) {
        auto nread = ::fread(buf, 1, count, stream_);
        if (ferror(stream_)) {
            throw errno_exception();
        }
        return nread;
    }

    ~pipe() {
        if (stream_ && !close()) {
            log::perror("Error while destroying an open pipe");
        }
    }
};

}} // namespace my::curl
