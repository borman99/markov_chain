#pragma once

#include <cerrno>
#include <cstring>
#include <stdexcept>

namespace my {

class errno_exception: public std::runtime_error {
    int errno_;
public:
    errno_exception():
        std::runtime_error(::strerror(errno)),
        errno_ {errno}
    { }
};

} // namespace my
