#pragma once

#include <fcntl.h>
#include <unistd.h>

#include <vector>

#include "errno_exception.hpp"
#include "log.hpp"

namespace my {

class subprocess_pipe {
    int fd_;
    int pid_;

public:
    subprocess_pipe(
            const char* program,
            std::vector<const char*> args)
    {
        int pipe_fds[2];
        if (::pipe(pipe_fds) < 0) {
            throw errno_exception();
        }

        auto pid = ::fork();
        if (pid == 0) {
            // In child

            // that's why `args` is passed by value
            args.push_back(nullptr);

            auto devnull_fd = ::open("/dev/null", O_RDWR);
            if (devnull_fd < 0
                    || ::dup2(devnull_fd, STDIN_FILENO) < 0
                    || ::dup2(devnull_fd, STDERR_FILENO) < 0
                    || ::dup2(pipe_fds[1], STDOUT_FILENO) < 0
                    || ::close(pipe_fds[0]) < 0
                    || ::close(pipe_fds[1]) < 0)
            {
                log::perror("Failed to prepare child");
                ::exit(1);
            }

            ::execvp(program, (char**) args.data());
            log::perror("Failed to exec child");
            ::exit(1);
        } else if (pid < 0) {
            throw errno_exception();
        }

        // In parent
        log::trace("Spawned PID ", pid);
        // close writing end
        if (::close(pipe_fds[1]) < 0) {
            throw errno_exception();
        }

        fd_ = pipe_fds[0];
        pid_ = pid;
    }
};

} // namespace my
