#pragma once

#include <boost/circular_buffer.hpp>
#include <boost/optional.hpp>
#include <boost/utility/string_ref.hpp>

#include "../curl.hpp"
#include "../debug_repr.hpp"
#include "../log.hpp"
#include "../word_iterator.hpp"

#include "word_trie.hpp"
#include "state.hpp"
#include "data.hpp"

namespace my { namespace model {

template<typename WordId, typename StateId, typename Count>
class basic_builder {
    using word_trie = basic_word_trie<WordId, StateId, Count>;
    using data = basic_data<WordId, StateId, Count>;

    word_trie ngrams_;
    data data_;

public:
    class failure: public std::runtime_error {
        // Inherit constructor
        using std::runtime_error::runtime_error;
    };

    basic_builder(size_t n):
        data_ (data::with_n(n))
    { }

    //! Fetch url, split into words and consume words
    void fetch_and_consume(boost::string_ref url) {
        log::info("FETCH ", url);

        auto stream = curl::pipe(url);
        auto words = make_word_iterator(
            [&] (auto buf, auto size) {
                return stream.read(buf, size);
            }
        );

        consume(words);

        if (!stream.close()) {
            // XXX: Better to have this in failure::what()
            auto message = std::string("Failed to download ");
            message.append(url.begin(), url.end());
            throw failure(message);
        }

        log::info("DONE ", url);
        log::debug("unique words in pool: ", data_.strings.size());
    }

    //! Add all n-grams from words sequence defined by iterator
    template<typename Words>
    void consume(Words& words) {
        using word_ngram = boost::circular_buffer<WordId>;

        // Use a circular buffer to track a window of size n
        auto ngram = word_ngram(data_.n);

        log::debug("begin consume");
        while (words.next()) {
            auto id = data_.strings.add(words.last());
            log::trace("> ", id, " ", words.last());

            ngram.push_back(id);
            if (ngram.full()) {
                // Only track full n-grams
                ngrams_.add_path(ngram);
            }
        }
        log::debug("end consume");
    }

    //! Reset state and return a data object
    data produce() {
        using std::swap;

        build_states();

        // Clear local state
        auto result = data::with_n(data_.n);
        swap(data_, result);
        ngrams_ = {};

        return result;
    }

private:
    //! Build an automaton from the word trie
    void build_states() {
        data_.states.reserve(ngrams_.size()); // NOTE: size() is O(N)
        data_.states.resize(1);
        ngrams_.id = 0;
        add_states_for_subtree(ngrams_);
        add_terminal_links(ngrams_);
    }

    //! Walk word trie via breadth-first search
    void add_states_for_subtree(word_trie& node) {
        auto& states = data_.states;
        auto probability = Count {0};

        auto next_state_id = static_cast<StateId>(states.size());

        // Allocate states for children
        states.resize(states.size() + node.links.size());

        states[node.id].reserve(node.links.size());
        for (auto& link : node.links) {
            auto word_id = link.first;
            auto& next_node = *link.second;
            next_node.id = next_state_id;

            probability += next_node.count;
            states[node.id].add_link(word_id, next_node.id, probability);

            add_states_for_subtree(next_node);
            next_state_id += 1;
        }

        states[node.id].total_probability = probability;
    }

    //! Walk word trie via breadth-first search
    void add_terminal_links(
            const word_trie& node,
            boost::optional<const word_trie&> suffix = boost::none)
    {
        auto& states = data_.states;

        if (node.links.empty()) {
            // Leaf node, walk suffix links
            auto probability = Count {0};
            log::ensure(!!suffix, "empty suffix in terminal node");

            for (auto& link : suffix->links) {
                auto word_id = link.first;
                auto& next_node = *link.second;

                probability += next_node.count;
                states[node.id].add_link(word_id, next_node.id, probability);
            }

            states[node.id].total_probability = probability;
        } else {
            // Intermediate node, descend
            for (auto& link : node.links) {
                auto word_id = link.first;
                auto& next_node = *link.second;
                auto next_suffix = suffix
                    ? suffix->try_walk(word_id)
                    : ngrams_;

                // Avoid creating bogus links to root
                if (next_suffix) {
                    add_terminal_links(next_node, next_suffix);
                }
            }
        }
    }
};

}} // namespace my::model
