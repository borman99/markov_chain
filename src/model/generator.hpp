#pragma once

#include <random>

#include "data.hpp"
#include "../log.hpp"

namespace my { namespace model {

template<typename WordId, typename StateId, typename Count>
class basic_generator {
public:
    using data = basic_data<WordId, StateId, Count>;

private:
    const data& data_;
    StateId state_id_;

public:
    basic_generator(const data& data__):
        data_ {data__},
        state_id_ {0}
    { }

    //! Generate a random word and transition to next state
    template<typename Generator>
    boost::optional<WordId> next(Generator& rng) {
        using uniform = std::uniform_int_distribution<Count>;

        if (state_id_ >= data_.states.size()) {
            // Invalid state
            return boost::none;
        }

        auto& state = data_.states[state_id_];
        if (state.links.empty()) {
            // No links: transition to invalid state
            state_id_ = data_.states.size();
            return boost::none;
        }

        auto distribution = uniform {1, state.total_probability};
        auto dice = distribution(rng);
        auto iter = state.links.lower_bound(dice);
        log::ensure(
            iter != state.links.end(),
            "invalid probability generated"
            // state.links is guaranteed to contain total_probability
            // as max key. thus, there must always be a match for
            // a value in range [1; total_probability]
        );

        state_id_ = iter->second.state;
        return iter->second.word;
    }

    //! Transition to given word. Used to initialize generator with
    //! a prior word sequence.
    void advance(WordId word_id) {
        log::trace("advance(", data_.strings[word_id], "), state = ", state_id_);

        if (state_id_ >= data_.states.size()) {
            // Invalid state
            log::trace("  in invalid state");
            return;
        }

        auto& state = data_.states[state_id_];
        auto iter = state.word_links.find(word_id);
        if (iter == state.word_links.end()) {
            // Invalid link: transition to invalid state
            log::trace("  no links -> invalid state");
            state_id_ = data_.states.size();
            return;
        }

        log::trace("  -> ", iter->second);

        state_id_ = iter->second;
    }
};


//! Helper function for argument type inference
template<typename WordId, typename StateId, typename Count>
basic_generator<WordId, StateId, Count>
make_generator(basic_data<WordId, StateId, Count>& data) {
    return {data};
}

}} // namespace my::model
