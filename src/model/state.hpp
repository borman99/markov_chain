#pragma once

#include <vector>

#include <boost/container/flat_map.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/utility.hpp>

#include "../boost_serialize_flat_map.hpp"

namespace my { namespace model {

template<typename WordId, typename StateId, typename Count>
struct basic_state {
    struct link {
        WordId word;
        StateId state;

        template<typename Archive>
        void serialize(Archive& ar, const unsigned int version) {
            (void) version;

            ar & word;
            ar & state;
        }
    };

    using Self = basic_state<WordId, StateId, Count>;
    using word_map = boost::container::flat_map<WordId, StateId>;
    using probability_map = boost::container::flat_map<Count, link>;

    //! Sum of words' probabilities
    Count total_probability;
    //! Maps each link's cumulative probability to a link, i.e.
    //! for x ~ uniform in [1; total_probability]
    //! P(links[x]) = link's learned probability
    probability_map links;
    //! Maps each possible word to a corresponding state
    //! Required for initializing generator with prior word sequence
    word_map word_links;


    void add_link(WordId word, StateId state, Count probability) {
        links.emplace(probability, link {word, state});
        word_links.emplace(word, state);
    }

    void reserve(size_t size) {
        links.reserve(size);
        word_links.reserve(size);
    }


    // Boost.Serialization glue code below

    template<typename Archive>
    void save(Archive& ar, const unsigned int version) const {
        (void) version;

        ar & total_probability;
        ar & links;
    }

    template<typename Archive>
    void load(Archive& ar, const unsigned int version) {
        (void) version;

        ar & total_probability;
        ar & links;

        word_links.reserve(links.size());
        for (auto& link : links) {
            word_links[link.second.word] = link.second.state;
        }
    }

    BOOST_SERIALIZATION_SPLIT_MEMBER()
};

}} // namespace my::model
