#pragma once

#include <memory>

#include <boost/container/flat_map.hpp>
#include <boost/optional.hpp>

namespace my { namespace model {

template<typename WordId, typename StateId, typename Count>
struct basic_word_trie {
    using Self = basic_word_trie<WordId, StateId, Count>;
    using link_map = boost::container::flat_map<
        WordId,
        std::unique_ptr<Self>
    >;

    StateId id;
    Count count;
    link_map links;


    //! Walk down the tree, create a node if needed, increment count
    Self& walk(WordId key) {
        auto& target = links[key];
        if (!target) {
            target = std::make_unique<Self>();
        }
        return *target;
    }

    //! Try to walk down the tree, return result node if exists
    boost::optional<const Self&> try_walk(WordId key) const {
        auto iter = links.find(key);
        if (iter == links.end()) {
            return boost::none;
        }

        return *iter->second;
    }

    //! Add a sequence of words (path), increment count at each step
    template<typename Path>
    void add_path(const Path& path) {
        auto* node = this;
        count += 1;
        for (auto id : path) {
            node = &node->walk(id);
            node->count += 1;
        }
    }

    //! Recursively calculate tree size
    //! NOTE: O(N) complexity
    size_t size() const {
        auto result = size_t {1};
        for (auto& link : links) {
            result += link.second->size();
        }
        return result;
    }
};

}} // namespace my::model
