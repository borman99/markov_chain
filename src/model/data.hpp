#pragma once

#include <vector>
#include <iostream>

#include <boost/serialization/vector.hpp>

#include "string_pool.hpp"
#include "state.hpp"

namespace my { namespace model {

template<typename WordId, typename StateId, typename Count>
struct basic_data {
    using Self = basic_data<WordId, StateId, Count>;
    using string_pool = basic_string_pool<WordId, Count>;
    using state = basic_state<WordId, StateId, Count>;

    size_t n;
    string_pool strings;
    std::vector<state> states;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version) {
        (void) version;

        ar & n;
        ar & strings;
        ar & states;
    }

    static
    Self with_n(size_t n) {
        return {n, {}, {}};
    }
};


template<typename WordId, typename StateId, typename Count>
std::ostream& operator <<(
        std::ostream& stream,
        const basic_data<WordId, StateId, Count>& data)
{
    stream << data.states.size() << '\n';
    auto id = size_t {0};
    for (auto& state : data.states) {
        stream
            << id << ' '
            << state.links.size() << ' '
            << state.total_probability << '\n';
        for (auto& link : state.links) {
            stream
                << "    "
                << data.strings[link.second.word] << ' '
                << link.second.state << ' '
                << link.first << '\n';
        }

        id += 1;
    }
    return stream;
}

template<typename WordId, typename StateId, typename Count>
std::istream& operator >>(
        std::istream& stream,
        basic_data<WordId, StateId, Count>& data)
{
    auto result = basic_data<WordId, StateId, Count> {};
    auto n_states = size_t {0};
    auto word = std::string {};

    stream >> n_states;
    result.states.resize(n_states);

    for (auto& state : result.states) {
        auto state_id = StateId {0};
        auto n_links = size_t {0};

        stream >> state_id >> n_links >> state.total_probability;
        // state_id is ignored
        state.reserve(n_links);
        for (auto i = size_t {0}; i < n_links; ++i) {
            auto probability = Count {0};
            stream >> word >> state_id >> probability;
            auto word_id = result.strings.add(word);

            state.add_link(word_id, state_id, probability);
        }
    }

    data = std::move(result);
    return stream;
}


}};
