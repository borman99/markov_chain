#pragma once

#include <cstdint>

#include <string>
#include <unordered_map>
#include <vector>
#include <limits>

#include <boost/optional.hpp>
#include <boost/serialization/split_member.hpp>

#include "../log.hpp"

namespace my {

template<typename WordId, typename Count>
class basic_string_pool {
    struct record {
        WordId id;
        // XXX: Could be used to sort ids by popularity
        // Currently unused
        Count count;
    };

    std::unordered_map<std::string, record> records_;
    std::vector<std::string> strings_;

public:
    WordId size() const {
        return static_cast<WordId>(strings_.size());
    }

    //! Get a word by its id
    const std::string& operator [](WordId index) const {
        return strings_.at(index);
    }

    //! Add a word, creating a new id or reusing existing one
    WordId add(const std::string& value) {
        auto iter = records_.find(value);
        if (iter != records_.end()) {
            iter->second.count += 1;
        } else {
            log::ensure(
                size() < std::numeric_limits<WordId>::max(),
                "string_pool::id_type overflow"
            );
            auto id = static_cast<WordId>(size());
            strings_.emplace_back(value);
            iter = records_.emplace(value, record {id, 0}).first;
        }
        return iter->second.id;
    }

    //! Find a word_id for a known word
    boost::optional<WordId> find(const std::string& value) const {
        auto iter = records_.find(value);
        if (iter != records_.end()) {
            return iter->second.id;
        }
        return boost::none;
    }

private:
    friend class boost::serialization::access;

    template<typename Archive>
    void save(Archive& ar, const unsigned int version) const {
        (void) version;

        ar & strings_;
    }

    template<typename Archive>
    void load(Archive& ar, const unsigned int version) {
        (void) version;

        ar & strings_;

        records_.reserve(strings_.size());
        auto index = WordId {0};
        for (auto& value : strings_) {
            records_[value] = record {index, 1};
            index += 1;
        }
    }

    BOOST_SERIALIZATION_SPLIT_MEMBER()
};

} //namespace my
