#pragma once

#include <cctype>

#include <memory>
#include <string>

#include "log.hpp"

namespace my {

template<typename Source>
class word_iterator {
    Source reader_;
    std::unique_ptr<char[]> buffer_;
    size_t buffer_size_;
    size_t buffer_end_;
    size_t buffer_pos_;
    std::string word_;
    bool in_word_;

public:
    word_iterator(Source reader, size_t buffer_size = 65536):
        reader_ {reader},
        buffer_ {std::make_unique<char[]>(buffer_size)},
        buffer_size_ {buffer_size},
        buffer_end_ {0},
        buffer_pos_ {0},
        in_word_ {false}
    { }

    const std::string &last() {
        return word_;
    }

    bool next() {
        word_.clear();
        in_word_ = false;
        do {
            if (next_in_buf()) {
                return true;
            }
        }
        while (read_more());
        return !word_.empty();
    }

private:
    bool isword(char c) {
        return isalnum(c) || c == '-' || c =='\'';
    }

    bool next_in_buf() {
        for (; buffer_pos_ < buffer_end_; buffer_pos_++) {
            const auto c = buffer_.get()[buffer_pos_];
            if (isword(c)) {
                in_word_ = true;
                word_ += tolower(c);
            } else {
                if (in_word_) {
                    in_word_ = false;
                    return true;
                }
            }
        }
        return false;
    }

    bool read_more() {
        buffer_end_ = reader_(buffer_.get(), buffer_size_);
        buffer_pos_ = 0;
        log::debug("read more -> ", buffer_end_);
        return buffer_end_ != 0;
    }
};

template<typename Source>
word_iterator<Source>
make_word_iterator(Source reader, size_t buffer_size = 65536) {
    return {reader, buffer_size};
}

} // namespace my
